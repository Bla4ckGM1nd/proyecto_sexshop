# __¡Advertencia!__

Este sitio solo es apto para aquellas personas que quieran expandir sus horizontes de conocimiento sexual tanto de si mismos como en pareja <3 

## _Qué es un SexShop?_

Si aún tienes la duda de que es un sexshop este es un almacén en el que venden cualquier tipo de artículo relacionado con sexualidad, como juguetes sexuales (como consoladores o vibradores), juegos eróticos, lencería y pornografía. En la mayoría de los países estos almacenes están restringidos para menores de edad, depende de la legislación específica de cada país, en Colombia debes ser mayor de edad (tener más de 18 años) para entrara a uno.

![SexShop](https://gitlab.com/Bla4ckGM1nd/proyecto_sexshop/-/blob/Rama_SCMR/assets/img/SexShop.jpg)

# Historia del sexshop
El primer sex shop del mundo se inauguró en 1962 por Beate Uhse en la ciudad alemana de Flensburgo, Alemania, muy cerca de la frontera con Dinamarca.

Las primeras tiendas eróticas se abrían en medio de un ambiente hostil o, por lo menos, poco familiarizado con este tipo de comercio por parte de las gentes del lugar, en el contexto de sociedades más conservadoras y menos abiertas a tratar en público temas de índole sexual; por estos motivos estos establecimientos solían ser lugares en cierta medida ocultos, sórdidos o tal vez les acompañaba el aura de ser algo pernicioso. Con el paso de las décadas, han ido cambiando y evolucionando tanto los productos que ofrecen las tiendas eróticas como la propia presentación de los mismos y del establecimiento en sí. El aperturismo de las sociedades occidentales, unido al avance de la tecnología y del estudio y desarrollo de la sexología ha diversificado la cantidad de juguetes sexuales disponibles en el mercado para ambos sexos. Si bien antaño los juguetes emulaban determinadas partes del cuerpo humano, en la actualidad las formas no son tan explícitas y se da paso a una nueva variedad de sensaciones que los juguetes convencionales no permitían. Las tiendas eróticas de nueva generación se conciben como lugares más abiertos, limpios, coloridos y bien iluminados, con una decoración y presentación que bien puede asemejarse a las de una perfumería. Sin embargo, a pesar de este aspecto más amable para el público general, todavía predomina la afluencia de personas en la franja de edad situada entre los 20 y los 40 años, si bien se va notando un aumento de clientes de mayor edad.

# Tipos de Tiendas Eróticas :3

Algunos sex shops ofrecen espectáculos eróticos, como proyecciones de cine porno, sesiones de estriptis en vivo y Peep Shows, que son actuaciones diseñadas para la gente que disfruta del voyerismo.

Los sex shops online disponen de mayor variedad de productos en sus catálogos porque no precisan de espacio físico para exhibirlos y, además de la comodidad de las compras por internet, aseguran la garantía para los clientes de permanecer en el anonimato.​ En España se reporta que han aumentado significativamente la cantidad de tiendas eróticas en línea.


### ¿Como se hizo la pagina web?

Se trabajó sobre un template de Bootstrap de la siguiente página: https://startbootstrap.com/.
Eligiendo asi entonces el template '[Bussines Casual](https://startbootstrap.com/theme/business-casual)' moficiando y adaptando los datos a este.

### GIT

Git es una herramienta que realiza una función del control de versiones de código de forma distribuida, de la que destacamos varias características:

1. Es muy potente
2. Fue diseñada por Linus Torvalds
3. No depende de un repositorio central
4. Es software libre
5. Con ella podemos mantener un historial completo de versiones
Podemos movernos, como si tuviéramos un puntero en el tiempo, por todas las revisiones de código y desplazarnos una manera muy ágil.

#### Funcion basica
![GIT](https://gitlab.com/Bla4ckGM1nd/proyecto_sexshop/-/blob/master/assets/img/git_vida_mrr.png)



